import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  foods: any[] = [
    {
      image_url : 'assets/imgs/1.png',
      food_name : 'biscake',
      food_desc : 'dummy dummy',
      price : 10,
      favor : 10,
      state : 'new'
    },
    {
      image_url : 'assets/imgs/2.png',
      food_name : 'sugar',
      food_desc : 'dummy dummy',
      price : 10,
      favor : 10,
      state : 'new'
    },
    {
      image_url : 'assets/imgs/3.png',
      food_name : 'pizza',
      food_desc : 'dummy dummy',
      price : 10,
      favor : 10,
      state : 'new'
    },
    {
      image_url : 'assets/imgs/4.png',
      food_name : 'sandwich',
      food_desc : 'dummy dummy',
      price : 10,
      favor : 10,
      state : 'dis count'
    },
    {
      image_url : 'assets/imgs/5.png',
      food_name : 'bread',
      food_desc : 'dummy dummy',
      price : 10,
      favor : 10,
      state : 'dis count'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
