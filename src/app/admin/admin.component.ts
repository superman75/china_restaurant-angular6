import {Component, OnInit} from '@angular/core';
import {LangChangeEvent, TranslateService} from "@ngx-translate/core";
import { FormBuilder,  FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {MyServiceService} from "../my-service.service";
import {ActivatedRoute} from "@angular/router";

declare let google;
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  loginForm : FormGroup;
  profileForm : FormGroup;
  passwordFormGroup: FormGroup;
  foodForm : FormGroup;

  validMsg : string;
  validPass : string;
  invalidMsg : string;
  confirmPassword : string;
  wrongPass : string;
  noMatchPass : string;
  logo : string;

  map : any;
  currentUser = {
    lat : 0,
    lon : 0
  };
  restPos = {
    lat : 0,
    lon : 0
  };
  private placesService : any;

  step = 1;
  result :any = {};
  selectFile = null;
  loginCredential = {
    email : '',
    password : ''
  };
  profile : any = {};

  newfood : any = {
    image_url : '',
    food_name : '',
    description : '',
    price : 0,
    when : 'lunch',
    restaurantid : ''
  };
  foods: any = [];

  changePass : boolean = false;
  currentIndex = 0;
  sub : any;
  constructor(private translate : TranslateService,
              private http: HttpClient,
              private route : ActivatedRoute,
              private fb : FormBuilder,
              private myservice : MyServiceService) {
    let self = this;
    translate.onLangChange.subscribe((event : LangChangeEvent)=>{
      self.validMsg = this.translate.instant('VALID');
      self.validPass = this.translate.instant('VALIDPASS');
      self.invalidMsg = this.translate.instant('INVALID');
      self.wrongPass = this.translate.instant('WRONGPASS');
      self.logo = this.translate.instant('LOGO');
      self.noMatchPass = this.translate.instant('NOMATCH');
    });
    this.loginForm = this.buildLoginForm();
    this.profileForm = this.buildProfileForm();
    this.foodForm = this.buildFoodForm();
  }

  buildLoginForm() {
    return this.fb.group({
      email: ['', [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6),Validators.maxLength(100)])]  ,
    })
  }
  buildFoodForm() {
    return this.fb.group({
      foodname : ['', Validators.required],
      price : [null, [Validators.required, Validators.pattern(/(^\d*\.?\d*[1-9]+\d*$)|(^[1-9]+\d*\.\d*$)/)]],
      desc : ['']
    })
  }
  buildProfileForm() {
    this.passwordFormGroup = this.fb.group({
      password: ['', Validators.compose([Validators.required, Validators.minLength(6),Validators.maxLength(100)])],
      repeatpassword: ['',Validators.compose([Validators.required, Validators.minLength(6),Validators.maxLength(100)])]
    }, {
      validator: RegistrationValidator.validate.bind(this)
    });
    return this.fb.group({
      checking : [''],
      resLogo : [''],
      resName : ['',Validators.required],
      email: ['', [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
      address : ['',Validators.required],
      phone : ['',[Validators.required, Validators.pattern(/^[2-9]\d{2}-\d{3}-\d{4}$/)]],

    })
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params=>{
      let restaurant = params['resname'];
      // console.log(restaurant);
      let sub1 = this.myservice.getProfileByName(restaurant)
        .subscribe(res=>{
          this.profile = res;
          this.confirmPassword = this.profile.password;

          this.getUserLocation();
          sub1.unsubscribe();
        })
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private getUserLocation() {
    /// locate the user
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.currentUser.lat = position.coords.latitude;
        this.currentUser.lon = position.coords.longitude;

        google.maps.event.addDomListener(window, 'load', this.initialize());
      }, error2 => console.log(error2));
    }
  }

  initialize() {
    let var_map = document.getElementById("map-container-9");

    let var_location = new google.maps.LatLng(this.currentUser.lat, this.currentUser.lon);

    let var_mapoptions = {
      center: var_location,
      zoom: 14
    };

    this.map = new google.maps.Map(var_map, var_mapoptions);

    this.placesService = new google.maps.places.PlacesService(this.map);
    let self = this;
    google.maps.event.addDomListener(this.map, 'click', function (event) {
      self.placesService.getDetails({placeId: event.placeId}, function (place, status) {
        if (status == 'OK') {
          self.restPos.lat = place.geometry.location.lat();
          self.restPos.lon = place.geometry.location.lng();
        } else {
          console.log(status);
        }
      })
    });
  }
  savePos(){
    if(!this.restPos.lat && !this.restPos.lon){
      alert(this.translate.instant('SELECTREST')); return;
    } else {
      this.profile.lat = this.restPos.lat;
      this.profile.lon = this.restPos.lon;
      let geocoder = new google.maps.Geocoder;
      let latlng = {lat: this.profile.lat , lng: this.profile.lon};
      let self = this;
      geocoder.geocode({'location': latlng}, function(results) {
        if (results[0]) {
          self.profile.address = results[0].formatted_address;
          document.getElementById('mapClose').click();

        } else {
          console.log('No results found');
        }
      });
    }
  }

  changeLang(){
    if(this.translate.currentLang=='en')this.translate.use('zh');
    else this.translate.use('en');
  }
  login() {
    if(this.loginCredential.email != this.profile.email || this.loginCredential.password != this.profile.password) {
      alert(this.translate.instant('CORRECTINFO'));
      return;
    }
    this.step = 2;
    let sub2 = this.myservice.getFoodByRestId(this.profile.id)
      .subscribe(res1=>{

        this.foods = res1;
        sub2.unsubscribe();
      });
  }

  delete(){
    // this.foods.splice(this.currentIndex,1);
    this.myservice.deleteFood(this.foods[this.currentIndex].id)
      .subscribe(res=>{

        let result : any = res;
        if(result.status == 'success'){
          this.foods.splice(this.currentIndex,1);
          this.currentIndex = 0;
        } else alert(this.translate.instant('SERVERERROR'));
      });

  }

  updateInfo(){
    if(this.confirmPassword!=this.profile.password){
      alert(this.translate.instant('NOMATCH'));
      console.log("Conform should be same to pass"); return;
    }
    console.log("Update");
    let self = this;
    let sub1 = this.myservice.updateProfile(this.profile)
      .subscribe(res=>{
        sub1.unsubscribe();
        document.getElementById('editModalClose').click();
      });
  }
  addNewFood(){
    this.newfood.restaurantId = this.profile.id;
    this.newfood.created_at = new Date().getTime();
    this.newfood.updated_at = new Date().getTime();
    this.myservice.createNewFood(this.newfood)
      .subscribe(res=>{
        let result : any = res;
        if(result.status='success'){
          this.foods.push(this.newfood);
          // if(res.status=='success'){
          document.getElementById('addModalId').click();
          this.newfood = {
            image_url : '',
            food_name : '',
            description : '',
            price : 0,
            when : 'lunch'
          };
        } else {
          alert(this.translate.instant('SERVERERROR'));
        }

        // }
      });
    console.log(this.newfood);

  }
  onImageSelected(event){
    this.selectFile = <File>event.target.files[0];
    const fd = new FormData();
    console.log(this.selectFile);
    if(this.selectFile.size>1000000){
      alert(this.translate.instant('LIMITERROR')); return;
    }
    fd.append("FileDatas", this.selectFile, this.selectFile.name);
    fd.append("description", this.newfood.description);
    this.myservice.uploadImageFile(fd).subscribe(data=>{
      this.result = data;
      this.newfood.image_url = this.result.message;
    },error=> alert(error.message));
  }
  onLogoSelected(event){
    this.selectFile = <File>event.target.files[0];
    const fd = new FormData();
    console.log(this.selectFile);
    if(this.selectFile.size>1000000){
      alert(this.translate.instant('LIMITERROR')); return;
    }
    fd.append("FileDatas", this.selectFile, this.selectFile.name);
    fd.append("description", "Profile Logo");
    this.myservice.uploadImageFile(fd).subscribe(data=>{
      this.result = data;
      this.profile.resLogo = this.result.message;
    });
  }
}


export class RegistrationValidator {
  static validate(registrationFormGroup: FormGroup) {
    let password = registrationFormGroup.controls.password.value;
    let repeatPassword = registrationFormGroup.controls.repeatpassword.value;

    if (repeatPassword.length <= 0) {
      return null;
    }

    if (repeatPassword !== password) {
      return {
        doesMatchPassword: true
      };
    }

    return null;

  }
}
