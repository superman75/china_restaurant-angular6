import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-orderlist',
  templateUrl: './orderlist.component.html',
  styleUrls: ['./orderlist.component.scss']
})
export class OrderlistComponent implements OnInit {
  restaurant = {
    lat : 51,
    long : 7.8,
    about : 'dummy dummy',
    address : 'PO Box 34628, Seattle, WA 98124-1628, US',
    email : 'restaurant@gmail.com',
    phone : '+1234567890'
  };
  totalPrice = 0;
  totalCount = 0;
  orders: any[] = [
    {
      image_url : 'assets/imgs/1.png',
      food_name : 'biscake',
      food_desc : 'dummy dummydummy dummydummy dummydummy dummydummy dummydummy dummydummy dummydummy dummy',
      price : 10,
      order_date : '08/30 : 12:30PM',
      count : 2,
      state : 'today'
    },
    {
      image_url : 'assets/imgs/2.png',
      food_name : 'sugar',
      food_desc : 'dummy dummy',
      price : 10,
      order_date : '08/30 : 12:30PM',
      count : 2,
      state : 'today'
    },
    {
      image_url : 'assets/imgs/3.png',
      food_name : 'pizza',
      food_desc : 'dummy dummy',
      price : 10,
      order_date : '08/30 : 12:30PM',
      count : 2,
      state : 'today'
    },
    {
      image_url : 'assets/imgs/4.png',
      food_name : 'sandwich',
      food_desc : 'dummy dummy',
      price : 10,
      order_date : '08/30 : 12:30PM',
      count : 2,
      state : 'ago'
    },
    {
      image_url : 'assets/imgs/5.png',
      food_name : 'bread',
      food_desc : 'dummy dummy',
      price : 10,
      order_date : '08/30 : 12:30PM',
      count : 2,
      state : 'later'
    }
  ];
  constructor(private _router: Router) { }

  ngOnInit() {
    this.updatePrice();
  }
  updatePrice() {
    this.totalCount = 0;
    this.totalPrice = 0;
    for (let i = 0; i < this.orders.length; i++) {
      this.totalCount += this.orders[i].count;
      this.totalPrice += this.orders[i].price * this.orders[i].count;
    }
  }
  increaseCount(i) {
    this.orders[i].count++;
    this.updatePrice();
  }
  decreaseCount(i) {
    if (this.orders[i].count > 0 ) {
      this.orders[i].count--;
    }
    this.updatePrice();
  }
  gotoSelectDate() {
    this._router.navigate(['selectdate']);
  }
}
