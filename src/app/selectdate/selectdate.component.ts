import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-selectdate',
  templateUrl: './selectdate.component.html',
  styleUrls: ['./selectdate.component.scss']
})
export class SelectdateComponent implements OnInit {

  selectedDate = '';
  selDate = '';
  selTime = '';
  constructor( private _location: Location) { }

  ngOnInit() {
    const now =  new Date();
    this.selDate = now.toISOString().split('T')[0];
    this.selTime = now.toLocaleTimeString('it-IT').slice(0, 5);
  }
  gotoUpdate() {
    this.selectedDate = this.selDate + ' ' + this.selTime;
    console.log(this.selectedDate);
    this._location.back();
  }
}
