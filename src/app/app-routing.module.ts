import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {GooglemapComponent} from "./googlemap/googlemap.component";
import {AdminComponent} from "./admin/admin.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'home/:resname',
    component: HomeComponent
  },
  {
    path : 'googlemap/:resname',
    component: GooglemapComponent,
  },
  {
    path : 'admin/:resname',
    component: AdminComponent,
  }
  // {
  //   path: 'favorites',
  //   component: FavoritesComponent
  // },
  // {
  //   path: 'aboutus',
  //   component: AboutusComponent
  // },
  // {
  //   path: 'contact',
  //   component: ContactComponent
  // },
  // {
  //   path: 'orderlist',
  //   component: OrderlistComponent
  // },
  // {
  //   path: 'ordernow',
  //   component: OrdernowComponent
  // },
  // {
  //   path: 'fooddetail',
  //   component: FooddetailComponent
  // },
  // {
  //   path: 'selectdate',
  //   component: SelectdateComponent
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
