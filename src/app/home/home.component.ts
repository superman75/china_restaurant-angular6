import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MyServiceService} from "../my-service.service";
import {Food} from "../models/food.model";
import { FormGroup} from "@angular/forms";
import {LangChangeEvent, TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  food : Food = new Food();
  filters: any[];
  foods: any = [
  ];
  profile : any = {};
  foods_with_images = [];
  foods_filter = [];
  currentIndex = 0;
  private sub: any;
  private sub2 : any;
  constructor(private _router: Router, private route : ActivatedRoute,private myservice : MyServiceService, private translate : TranslateService) {
    console.log(translate.currentLang);

    translate.onLangChange.subscribe((event : LangChangeEvent)=>{
      this.filters =  [
        {
          value: 1,
          viewValue: this.translate.instant('ALL')
        },
        {
          value: 2,
          viewValue: this.translate.instant('LUNCH')
        },
        {
          value: 3,
          viewValue: this.translate.instant('DINNER')
        },
        {
          value: 4,
          viewValue: this.translate.instant('POPULAR')
        },
        {
          value: 5,
          viewValue: this.translate.instant('SPECIAL')
        }
      ];
    });
    translate.use('en'); 

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params=>{
      let restaurant = params['resname'];
      console.log(restaurant);
      let sub1 = this.myservice.getProfileByName(restaurant)
        .subscribe(res=>{
          if(!res) {
            alert(this.translate.instant('SELECTREST')); return;
          }
          this.profile = res;
          console.log(this.profile);
          this.sub2 = this.myservice.getFoodByRestId(this.profile.id)
            .subscribe(res1=>{
              console.log(res1);
              this.foods = res1;
              this.foods_with_images = this.foods.filter(item=> item.image_url!='');
              this.foods_filter = this.foods;

            });
          sub1.unsubscribe();
        })
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
    this.sub2.unsubscribe();
  }
  gotoDetail(i) {
    this._router.navigate(['fooddetail']);
  }

  callViaPhone(){
    console.log('call phone');

  }
  openMap(){
    console.log('open map');
    this._router.navigate(['googlemap/', this.profile.resName]);

  }
  filterBy(index : number){
    if(index == 1) {
      this.foods_filter = this.foods; return;
    }
    let fil = '';
    switch (index){
      case 2: fil = 'lunch';break;
      case 3: fil = 'dinner'; break;
      case 4: fil = 'popular';break;
      default : fil = 'special'
    }
    this.foods_filter = this.foods.filter(item=>item.when.toLowerCase().indexOf(fil.toLowerCase())>-1);
  }
  saveCallHistory(){

  }
  changeLang(){
    if(this.translate.currentLang=='en')this.translate.use('zh');
    else this.translate.use('en');
  }

  gotoAdmin() {
    this._router.navigate(['admin/', this.profile.resName]);
  }
}
