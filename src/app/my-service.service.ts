import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Food} from "./models/food.model";


const base_url = 'http://192.168.2.21:8080/';
const contact_api = base_url + 'contacts/';
const profile_api = base_url + 'profile/';
const myorder_api = base_url + 'myorders/';
const food_api = base_url + 'foods/';
const upload_api = base_url + 'upload';

@Injectable({
  providedIn: 'root'
})

export class MyServiceService {

  constructor(private http: HttpClient) {
  }

  //GET REQUESTS

  getAllFoods(){
    return this.http.get(food_api + 'all');
  }

  getAllContacts(){
    return this.http.get(contact_api + 'all');
  }

  getAllMyOrders() {
    return this.http.get(myorder_api + 'all');
  }

  getAllProfiles() {
    return this.http.get(profile_api + 'all');
  }

  getFoodById(id : string) {
    return this.http.get(food_api + 'id/' + id);
  }
  getFoodByRestId(restId : string){
    return this.http.get(food_api + 'restId/' + restId);
  }

  getContactById(id : string){
    return this.http.get(contact_api + id);
  }

  getProfileByEmail(email : string){
    return this.http.get(profile_api  + 'email/' + email);
  }

  getProfileByAddress(address : string) {
    return this.http.get(profile_api + 'address/' + address);
  }

  getProfileByName(resName : string) {
    return this.http.get(profile_api + 'resName/' + resName);
  }


  // DELETE REQUESTS
  deleteFood(id : string){
    return this.http.delete( food_api + id);
  }

  deleteProfile(id : string){
    return this.http.delete(profile_api + id);
  }

  deleteContact(id : string){
    return this.http.delete(contact_api + id);
  }

  deleteMyOrder(id : string){
    return this.http.delete(myorder_api + id);
  }

  // PUT REQUESTS
  createNewFood(food){
    return this.http.put(food_api, food);
  }

  createNewMyorder(myorder){
    return this.http.put(myorder_api, myorder);
  }

  createNewContact(contact){
    return this.http.put(contact_api, contact);
  }

  createNewProfile(profile) {
    return this.http.put(profile_api, profile);
  }

  // POST REQUEST

  updateFood(food){
    return this.http.post(food_api, food);
  }

  updateMyorder(myorder){
    return this.http.post(myorder_api, myorder);
  }

  updateProfile(profile){
    return this.http.post(profile_api, profile);
  }

  updateContact(contact){
    return this.http.post(contact_api, contact);
  }

  uploadImageFile(fd : FormData)
  {
    return this.http.post(upload_api, fd);
  }
}
