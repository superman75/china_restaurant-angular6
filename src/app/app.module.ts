import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule, MatSelectModule,
  MatFormFieldModule, MatInputModule, MatButtonToggleModule, MatDatepickerModule
} from '@angular/material';
import { AgmCoreModule} from '@agm/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FavoritesComponent } from './favorites/favorites.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactComponent } from './contact/contact.component';
import { OrderlistComponent } from './orderlist/orderlist.component';
import { OrdernowComponent } from './ordernow/ordernow.component';
import { FooddetailComponent } from './fooddetail/fooddetail.component';
import { SelectdateComponent } from './selectdate/selectdate.component';
import { MyServiceService} from "./my-service.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { GooglemapComponent } from './googlemap/googlemap.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import { AdminComponent } from './admin/admin.component';
// MDB Angular Pro
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MainNavComponent,
    FavoritesComponent,
    AboutusComponent,
    ContactComponent,
    OrderlistComponent,
    OrdernowComponent,
    FooddetailComponent,
    SelectdateComponent,
    GooglemapComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatListModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCm7rSYPVVqY0W2TAO0d8h8J_PYmH8cKqw'
    }),
    MDBBootstrapModule.forRoot(),
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    MyServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
