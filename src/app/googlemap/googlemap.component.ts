import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MyServiceService} from "../my-service.service";


@Component({
  selector: 'app-googlemap',
  templateUrl: './googlemap.component.html',
  styleUrls: ['./googlemap.component.scss']
})
export class GooglemapComponent implements OnInit {

  currentUser = {
    lat : 51,
    lon : 7.8
  };
  profile : any = {};
  private sub: any;
  constructor(private route : ActivatedRoute,private myservice : MyServiceService) {

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params=>{
      let restaurant = params['resname'];
      console.log(restaurant);
      let sub1 = this.myservice.getProfileByName(restaurant)
        .subscribe(res=>{
          this.profile = res;
          console.log(this.profile);
          this.getUserLocation();
          sub1.unsubscribe();
        })
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  private getUserLocation() {
    /// locate the user
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.currentUser.lat = position.coords.latitude;
        this.currentUser.lon = position.coords.longitude;

      });
    }
  }
}
