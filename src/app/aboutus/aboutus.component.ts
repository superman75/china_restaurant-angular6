import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss']
})
export class AboutusComponent implements OnInit {

  restaurant = {
    lat : 51,
    long : 7.8,
    about : 'dummy dummy',
    address : 'PO Box 34628, Seattle, WA 98124-1628, US',
    email : 'restaurant@gmail.com',
    phone : '+1234567890'
  };
  constructor() { }

  ngOnInit() {

  }

}
