export class Food {
  id : string;
  image_url  :string;
  food_name : string;
  price : number;
  description : string;
  state : string;
  when : string;
  created_at : Date;
  updated_at : Date;
  restaurantId : string;
}
