export class Contact {
  id : string;
  email : string;
  title : string;
  content : string;
  created_at : Date;
  restaurant_id : string;
}
