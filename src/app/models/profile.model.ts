export class Profile {
  id : string;
  email : string;
  phone : string;
  about : string;
  address : string;
  lat : number;
  lon : number;
  password : string;
  resName : string;
  resLogo : string;
  zipcode : string;
}

