import {order} from "./order.model";

export class Myorder {
  id :string;
  orders: order[];
  user_name : string;
  user_phone : string;
  created_at  : Date;
  restaurantId : string;
}
